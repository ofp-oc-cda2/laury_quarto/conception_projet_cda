# conception_projet_cda

## 1. Techno

- Symfo (pour le backend de l'API)
- Vue.js (pour le front de l'appli web)
- Flutter (pour le front de l'appli mobile)

## 2. Description projet

Application locale (Besançon et alentours) pour de la vente entre particulier de créations fait maison, ex. savons, déco, couture...
Publications type leboncoin / etsy

### 2.1. Fonctionnalités (User story)

#### Administrateur :
Il peut voir les profils de tout le monde (public ou privé), envoyer des messages à n'importe quel utilisateur et les bannir si besoin.
Il peut également désigner d'autres utilisateurs pour qu'ils deviennent eux-mêmes des administrateurs.

#### Utilisateur acheteur/vendeur:
L'utilisateur de façon général peut créer modifier (mot de passe, email, adresse, rendre son profil public ou privé ...) et supprimer son compte.
Il peut réaliser une recherche sur l'application pour retrouver des vendeurs en particulier ou des produits (recherche par nom ou catégorie).
Il peut signaler ou suivre un autre utilisateur.
Il peut recevoir s'il le souhaite des notifications lorsque des vendeurs qu'il suit postent des produits.
Il peut lister les produits des différents utilisateurs suivis sur une même page.
Il peut communiquer avec les utilisateurs.
Le nombre d'échange réalisé, une moyenne sur 5 et les commentaires d'autre utilisateur sur lui s'affichent sur son profil.

#### Utilisateur vendeur :
Il peut créer, modifier (titre, description, images, prix, catégorie, les rendre visibles ou non) et supprimer des produits.
Il peut bannir des utilisateurs qui le suivent s'il le souhaite.

## 3. MCD
```mermaid
classDiagram
    Utilisateur "1" <--> "*" Discussion
    Utilisateur "1" <--> "*" Discussion
    Utilisateur "1" <--> "*" Message
    Utilisateur "1" <--> "*" Message
    Discussion "1" <--> "*" Message
    infoVendeur "1" --> "*" Produit
    Utilisateur "1" <--> "*" CommentaireVendeur
    Utilisateur "1" <--> "*" Abonnement
    infoVendeur "1" <--> "*" Abonnement
    CommentaireVendeur "*" --> "1" infoVendeur
    Image "*" --> "1" Produit
    CategorieProduit "*" --> "1" Produit
    Utilisateur "1" --> "*" Commande
    Commande "1" --> "*" Produit
    Commande "1" --> "*" Statut
    Utilisateur "1" --> "1" infoVendeur

    class Abonnement {
      utilisateurId: int fk
      infoVendeurId: int fk
    }
    class infoVendeur {
      id: int PK
      utilisateurId: int fk
      profilPublic: bool
      description: text
    }
    class Commande {
      id: int PK
      utilisateurId: int fk
      produitId: int fk
      statutId: int fk
      date: datetime
      quantite: int
      prix: int
      infovendeurId: int fk
    }

    class Image {
      id: int PK
      filepath: varchar
    }
    class Utilisateur {
      id: int PK
      nom: varchar
      prénom: varchar
      pseudo: varchar
      rôleId: int fk
      email: varchar
      password: varchar
      codepostal: int
      ville: varchar
      adresse: varchar 
      filepath: varchar
      vendeur: bool
      updateAt: datetime
    }
    class CommentaireVendeur {
      id: int PK
      utilisateurId: int fk
      vendeurId: int fk
      note: int
      publication: datetime
      commentaire: text
    }
    class Message {
      id: int PK
      discussionId: int fk
      de_userId: int fk
      a_userId: int fk
      publication: datetime
      message: text
      non_lu: bool
    }
    class Discussion {
      id: int PK
      UtilisateurA_id: int fk
      UtilisateurB_id: int fk
    }
    class Produit {
      id: int PK
      categorieId: int fk
      vendeurId: int fk
      titre: varchar
      description: text
      prix: int
      afficher: bool
      quantite: integer
      publication: datetime
    }
    class CategorieProduit {
      id: int PK
      nom: varchar
    }
    class Statut {
      id: int PK
      nom: varchar
    }
            
```

## 4. Maquettage - logiciel Figma
<a href="https://www.figma.com/file/Trhp0l8CWPixYzpLQdNabQ/Blocal?node-id=0%3A1 lien figma">Lien (en cours de réalisation)</a>




